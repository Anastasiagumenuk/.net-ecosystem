﻿using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System;
using System.Runtime.Serialization;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {

            int i;
            int j = 1;

            Console.WriteLine("1.Вивести на екран поточний баланс Паркiнгу");
            Console.WriteLine("2.Вивести на екран суму зароблених коштiв за поточний перiод (до запису у лог)");
            Console.WriteLine("3.Вивести на екран кiлькiсть вiльних мiсць на паркуваннi (вiльно X з Y)");
            Console.WriteLine("4.Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод (до запису у лог)");
            Console.WriteLine("5.Вивести на екран iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log)");
            Console.WriteLine("6.Вивести на екран список Тр. засобiв , що знаходяться на Паркiнгу");
            Console.WriteLine("7.Поставити Транспортний засiб на Паркiнг");
            Console.WriteLine("8.Забрати Транспортний засiб з Паркiнгу");
            Console.WriteLine("9.Поповнити баланс конкретного Тр. засобу");

            Console.WriteLine();
            Console.Write("Виберiть операцiю : ");
            i = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();


            TimerService WithdrawTimer = new TimerService(Settings.PaymentPeriodSeconds, true);
            TimerService LogTimer = new TimerService(Settings.RecordingLogPeriodSeconds, false);
            ParkingService parkingService = new ParkingService(WithdrawTimer, LogTimer, LogTimer.LogService);

            Vehicle bus = new Vehicle("AS-2345-K", VehicleType.Bus, 8);
            Vehicle motorcycle = new Vehicle("LP-9815-PH", VehicleType.Motorcycle, 8);
            Vehicle pasengerCar = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.PassengerCar, 8);

            parkingService.AddVehicle(bus);
            parkingService.AddVehicle(motorcycle);
            parkingService.AddVehicle(pasengerCar);


          
                switch (i)
                {
                    case 1:
                        Console.WriteLine("Поточний баланс Паркiнгу : " + parkingService.Parking.Balance);
                        break;
                    case 2:
                        Console.WriteLine("Сума зароблених коштiв за поточний перiод : ");
                        break;
                    case 3:
                        Console.WriteLine("Кiлькiсть вiльних мiсць на паркуваннi : " + parkingService.GetFreePlaces());
                        break;
                    case 4:
                        Console.WriteLine("Усi Транзакцiї Паркiнгу за поточний перiод : ");
                        break;
                    case 5:
                        Console.WriteLine("Iсторiя Транзакцiй(данi з файлу Transactions.log) : ");
                        break;
                    case 6:
                        Console.WriteLine("Список Тр. засобiв , що знаходяться на Паркiнгу) : ");
                        break;
                    case 7:
                        Console.WriteLine("Поставити Транспортний засiб на Паркiнг : ");
                        break;
                    case 8:
                        Console.WriteLine("Забрати Транспортний засiб з Паркiнгу : ");
                        break;
                    case 9:
                        Console.WriteLine("Поповнити баланс конкретного Тр. засобу : ");
                        break;
                    default:
                        Console.WriteLine("Введiть корректний символ!");
                        break;
                }






            //Vehicle vehicle1 = new Vehicle("LP-9815-PH", VehicleType.Motorcycle, 8);
            //Console.WriteLine(Vehicle.IsValidId(vehicle1.Id));

            //parkingService.AddVehicle(vehicle);
            //parkingService.AddVehicle(vehicle1);

            //Console.WriteLine(parkingService.GetCapacity());
            //Console.WriteLine(parkingService.GetFreePlaces());
            //Console.WriteLine(parkingService.GetBalance());
            //var v = parkingService.GetVehicles();

            //foreach (Vehicle veh in v)
            //{
            //    Console.WriteLine(veh.Id);
            //}

            //Vehicle vehicle2 = new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.PassengerCar, 8);
            //parkingService.AddVehicle(vehicle2);



            Console.ReadKey();
        }
    }
}
