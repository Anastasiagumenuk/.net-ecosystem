﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public string Id { get; }
        public DateTime TimeOfTransaction { get; set; }

        public TransactionInfo(DateTime _TimeOfTransaction, string _Id, decimal _Sum)
        {
            TimeOfTransaction = _TimeOfTransaction;
            Id = _Id;
            Sum = _Sum;
        }

    }
}