﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public int ParkingCapacity { get; set; }
        public List<TransactionInfo> Transactions { get; set; }

        protected Parking(decimal _Balance, int _ParkingCapacity)
        {
            Balance = _Balance;
            ParkingCapacity = _ParkingCapacity;
            Vehicles = new List<Vehicle>(ParkingCapacity);
            Transactions = new List<TransactionInfo>();
            
        }

        public static Parking GetInstance(decimal _Balance, int _ParkingCapacity)
        {
            if (instance == null)
            {
                instance = new Parking(_Balance, _ParkingCapacity); 
            }
            return instance;
        }

    }
}
