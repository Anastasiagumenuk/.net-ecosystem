﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        
        [RegularExpression("[A-Z]{2}-[1-9]{4}-[A-Z]{2}", ErrorMessage = "Id shoud have format : XX-YYYY-XX")]
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string _Id, VehicleType _VehicleType, decimal _Balance)
        {
            if (IsValidId(_Id))
            {
                Id = _Id;
            }
            else
            {
                Id = Vehicle.GenerateRandomRegistrationPlateNumber();
            }

            VehicleType = _VehicleType;
            Balance = _Balance;
        }

        static string GenerateTwoLetters()
        {
            Random random = new Random();

            string twoLetters = "";
            string lettersForRandom = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            for(int i = 0; i < 2; i++)
            {
                twoLetters += lettersForRandom[random.Next(0, lettersForRandom.Length)];
            }

            return twoLetters;
        }

        static string GenerateFourDigits()
        {
            Random random = new Random();
            string fourDigits = "";

            for (int i = 0; i < 4; i++)
            {
                fourDigits += random.Next(0, 10).ToString();
            }

            return fourDigits;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string randomRegistrationPlateNumber;

            randomRegistrationPlateNumber = GenerateTwoLetters() + "-" + GenerateFourDigits() + "-" + GenerateTwoLetters();

            return randomRegistrationPlateNumber;
        }

        public static bool IsValidId(string Id)
        {
            if (string.IsNullOrWhiteSpace(Id))
                return false;

            try
            {
                return Regex.IsMatch(Id,
                    "^[A-Z]{2}-[1-9]{4}-[A-Z]{2}$",
                    RegexOptions.Singleline, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
