﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Dynamic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance = 0;
        public static int ParkingCapacity = 10;
        public static int PaymentPeriodSeconds = 5000;
        public static int RecordingLogPeriodSeconds = 60000;
        public static decimal PenaltyRatio = 2.5M;
        
     
        public static decimal PaymentTariff(VehicleType vehicleType)
        {
            decimal Tarrif = 0;

            switch (vehicleType)
            {
                case VehicleType.Bus:
                    Tarrif = 3.5M;
                    break;
                case VehicleType.Motorcycle:
                    Tarrif = 1.0M;
                    break;
                case VehicleType.PassengerCar:
                    Tarrif = 2.0M;
                    break;
                case VehicleType.Truck:
                    Tarrif = 5.0M;
                    break;
            }

            return Tarrif;
        } 
    }
}
