﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private ElapsedEventArgs e;
        public Timer Timer { get; set; }
        public double Interval { get; set; }
        bool WithdrawOrLog { get; set; }

        public event ElapsedEventHandler Elapsed;
        public Parking Parking { get; set; }
        public TransactionInfo TransactionInfo { get; set; }
        public LogService LogService { get; set; }
    
        public TimerService(double _Interval, bool _WithdrawOrLog)
        {
            Interval = _Interval;
            WithdrawOrLog = _WithdrawOrLog;
            
            Timer = new Timer();
            Timer.Interval = Interval;
            Timer.AutoReset = true;

            LogService = new LogService(@"C:\Users\User\Documents\.net-ecosystem\CoolParking\CoolParking\Logs\Transactions.log");
            Parking = Parking.GetInstance(Settings.Balance, Settings.ParkingCapacity);
          

            if (WithdrawOrLog == true)
            {
                Timer.Elapsed += WithdrawTimerElapsed;
            }
            else
            {
                Timer.Elapsed += LogTimerElapsed;
            }

        }

        public void Dispose()
        {
            Elapsed?.Invoke(this,e);
        }

        public void Start()
        {
            Timer.Enabled = true;
        }

        private void WithdrawTimerElapsed(object sender, ElapsedEventArgs e)
        {
            decimal notEnoughSum;
            decimal enoughSum;

            Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine("Parking Balance : " + Parking.Balance);

            foreach (Vehicle vehicle in Parking.Vehicles)
            {
                notEnoughSum = 0;
                enoughSum = 0;

                if (vehicle.Balance >= 0)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                //Console.WriteLine("Vehicle Id : " + vehicle.Id + "  Balance : " + vehicle.Balance);

                if (vehicle.Balance >= Settings.PaymentTariff(vehicle.VehicleType))
                {
                    vehicle.Balance -= Settings.PaymentTariff(vehicle.VehicleType);
                    Parking.Balance += Settings.PaymentTariff(vehicle.VehicleType);

                    TransactionInfo = new TransactionInfo(e.SignalTime, vehicle.Id, Settings.PaymentTariff(vehicle.VehicleType));
                    Parking.Transactions.Add(TransactionInfo);
                }
                else if (vehicle.Balance > 0 && vehicle.Balance < Settings.PaymentTariff(vehicle.VehicleType))
                {
                    notEnoughSum = Settings.PaymentTariff(vehicle.VehicleType) - vehicle.Balance;
                    enoughSum = vehicle.Balance;
                    vehicle.Balance -= enoughSum + notEnoughSum * Settings.PenaltyRatio;
                    Parking.Balance += Settings.PaymentTariff(vehicle.VehicleType);

                    TransactionInfo = new TransactionInfo(e.SignalTime, vehicle.Id, enoughSum + notEnoughSum * Settings.PenaltyRatio);
                    Parking.Transactions.Add(TransactionInfo);

                }
                else if (vehicle.Balance <= 0 && vehicle.Balance < Settings.PaymentTariff(vehicle.VehicleType))
                {
                    vehicle.Balance -= Settings.PaymentTariff(vehicle.VehicleType) * Settings.PenaltyRatio;
                    Parking.Balance += Settings.PaymentTariff(vehicle.VehicleType) * Settings.PenaltyRatio;

                    TransactionInfo = new TransactionInfo(e.SignalTime, vehicle.Id, Settings.PaymentTariff(vehicle.VehicleType) * Settings.PenaltyRatio);
                    Parking.Transactions.Add(TransactionInfo);
                }
           
            }

            Console.ResetColor();
            Console.WriteLine();
        }

        private void LogTimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                foreach (var transaction in Parking.Transactions)
                {
                    LogService.Write(transaction.TimeOfTransaction + "   " + transaction.Id + "   " + transaction.Sum);
                }
            }
            catch(InvalidOperationException ex)
            {
                foreach (var transaction in Parking.Transactions)
                {
                    LogService.Write(transaction.TimeOfTransaction + "   " + transaction.Id + "   " + transaction.Sum);
                }
            }

         
        }

        public void Stop()
        {
            Timer.Stop();
        }
    }
}