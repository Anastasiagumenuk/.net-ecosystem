﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogFilePath;
        public string LogPath => LogFilePath;

        public LogService(string _LogFilePath)
        {
            LogFilePath = _LogFilePath;
            File.WriteAllText(LogFilePath, string.Empty);

        }

        public string Read()
        {
            string actual = "";

            try
            {    
                using (StreamReader r = File.OpenText(LogFilePath))
                {
                     actual = r.ReadToEnd();
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("The file is not found");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return actual;
        }

        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(LogFilePath, true))
                {
                    file.WriteLine(logInfo);
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("The file is not found");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}