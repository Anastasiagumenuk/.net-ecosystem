﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public ILogService LogService { get; set; }
        public ITimerService WithdrawTimer { get; set; }
        public ITimerService LogTimer { get; set; }
        public Parking Parking { get; set; }
        
        public ParkingService(ITimerService _WithdrawTimer, ITimerService _LogTimer, ILogService _LogService)
        {
            WithdrawTimer = _WithdrawTimer;
            LogTimer = _LogTimer;
            LogService = _LogService;

            Parking = Parking.GetInstance(Settings.Balance, Settings.ParkingCapacity);
        
            WithdrawTimer.Start();
            LogTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            Parking.Vehicles.Add(vehicle);  
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            int FreePlaces;
            FreePlaces = Parking.ParkingCapacity - Parking.Vehicles.Count;

            return FreePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
           ReadOnlyCollection<Vehicle> readOnlyVehicles = new ReadOnlyCollection<Vehicle>(Parking.Vehicles);

           return readOnlyVehicles;
        }

        public string ReadFromLog()
        {
            throw new System.NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            foreach(Vehicle vehicle in Parking.Vehicles)
            {
                if(vehicle.Id == vehicleId)
                {
                    Parking.Vehicles.Remove(vehicle);
                }
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            foreach(Vehicle vehicle in Parking.Vehicles)
            {
                if(vehicle.Id == vehicleId)
                {
                    vehicle.Balance += sum;
                }
            }
        }
    }
}